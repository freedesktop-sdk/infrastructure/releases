# Configuration for Freedesktop SDK ostree server
---
- hosts: releases
  gather_facts: false
  become: yes
  become_method: sudo
  vars:
    source_path: /root/src
    srv_path: "/srv/releases"
    data_path: "{{srv_path}}/repos"
    releases_repo: "{{data_path}}/releases"
    releases_lockfile: "/home/releases/lock"
    incoming_dir: "{{srv_path}}/incoming"
    ssl_url: releases.freedesktop-sdk.io
    public_pull_url: "https://{{ssl_url}}"
    dehydrated_domain_path: /etc/dehydrated/
    dehydrated_well_known: /var/www/dehydrated/
    certs_path: /etc/dehydrated/certs/
    account_keys: /etc/dehydrated/accounts/
    # use binary package of flat-manager. This is the same version as used by flathub
    # https://github.com/flathub/ansible-playbook/blob/master/roles/repo-manager/defaults/main.yml#L13
    flat_manager_version: "0.3.7"
    flat_manager_checksum: sha256:e3d61f312a5f9fc057b038db499c0d8de706b431043de89045bde9f07bdc29f6

  handlers:
  - name: reload sshd
    service: name=sshd state=reloaded

  - name: restart nginx
    service: name=nginx state=restarted

  - name: restart flat-manager
    service: name=flat-manager state=restarted

  - name: restart postgresql
    service: name=postgresql state=restarted

  tasks:
  # Disabling SELinux
  - name: disable SELinux on subsequent boots
    selinux: state=disabled
  - name: check if SELinux is disabled
    command: getenforce
    register: result
  - name: check if we need to manually disable SELinux
    command: setenforce 0
    when: result.stdout != "Disabled"

  # Install dependencies.
  - name: install packages
    dnf: name={{item}} state=latest
    with_items:
    - bubblewrap
    - git
    - ostree
    - python3-gobject
    - python3-pip
    - nginx
    - dehydrated
    - flatpak
    - postgresql-server
    - python3-psycopg2

  - name: Check postgres has been initialized
    stat:
      path: /var/lib/pgsql/data/postgresql.conf
    register: pgsql_data

  - name: configure postgresql
    command: /usr/bin/postgresql-setup --initdb
    when: not pgsql_data.stat.exists
    notify:
    - restart postgresql

  - name: Make sure postgresql is running
    systemd:
      state: started
      name: postgresql

  # Install flat-manager, for uploading to the releases repo
  - name: download flat-manager binary release
    get_url:
      url: "https://github.com/flatpak/flat-manager/releases/download/{{flat_manager_version}}/flat-manager"
      checksum: "{{flat_manager_checksum}}"
      dest: "/usr/bin/flat-manager"
      mode: 0775
    notify:
    - restart flat-manager

  # User configuration.
  - name: create users
    user: name={{item}}
    with_items:
    - releases

  - name: create postgres user
    postgresql_user:
      name: releases
    become_user: postgres

  - name: create postgres database
    postgresql_db:
      name: flatmanager
      owner: releases
    become_user: postgres


  - name: permit http port
    ansible.posix.firewalld:
      service: http
      permanent: yes
      state: enabled
  - name: permit https port
    ansible.posix.firewalld:
      service: https
      permanent: yes
      state: enabled

  - name: create srv path
    file: path={{srv_path}} state=directory

  # Create the repositories
  - name: releases directory
    file: mode=755 recurse=yes owner=releases group=releases path={{releases_repo}} state=directory
  - name: releases repository
    command: ostree init --repo={{releases_repo}} --mode=archive-z2
    become_user: releases
    args:
      creates: "{{releases_repo}}/config"

  # directory to hold the releases until they are imported to the repository
  - name: create incoming directory
    file:
      path: "{{incoming_dir}}"
      state: directory
      owner: releases

  - name: configure dehydrated
    template: src=./etc/dehydrated/config.sh dest=/etc/dehydrated/config

  - name: create our domain.txt
    template: src=./etc/dehydrated/domains.txt dest="{{dehydrated_domain_path}}/domains.txt"

  - name: create www directory
    file:
      path: /var/www/dehydrated/
      state: directory
      recurse: yes

  - name: Ensure that the existing certificate is still valid 2 weeks (1209600 seconds) from now
    openssl_certificate:
      path: "{{certs_path}}{{ssl_url}}/cert.pem"
      provider: assertonly
      valid_in: 1209600
    ignore_errors: yes
    register: ssl_valid

  # Configure Nginx to serve over HTTP so Lets Encrypt can access the server
  # in order to retrieve the request token and validate our SSL request.
  # This is only needed if we do not find a valid SSL certificate on the
  # machine at startup.
  - name: install nginx configuration
    template: src=./etc/nginx/nginx-http.conf.in dest=/etc/nginx/nginx.conf
    when: ssl_valid is failed
    notify:
    - restart nginx

  - name: register dehydrated
    command: dehydrated --register --accept-terms
    when: ssl_valid is failed

  - name: start dehydrated
    command: dehydrated -c
    when: ssl_valid is failed

  - name: copy over root lets encryt cert
    copy:
      src: ./etc/certs/le-root.pem
      dest: "{{certs_path}}le-root.pem"

  # Test to ensure that dehydrated was run and didn't error. We can then
  # naively assume that valid SSL certs will exist, so we can reconfigure
  # nginx to serve via HTTPS.
  - name: install nginx configuration
    template: src=./etc/nginx/nginx-ssl.conf.in dest=/etc/nginx/nginx.conf
    notify:
    - restart nginx

  # SSH daemon configuration
  - name: sshd configuration
    blockinfile:
      path: /etc/ssh/sshd_config
      block: |
        Match user releases
            PasswordAuthentication no
    notify:
    - reload sshd

  - name: Include secret variables
    include_vars: secret.yml

  - name: import gpg key
    become_user: releases
    shell: echo "{{sign_key}}" | base64 --decode | gpg2 --import

  # flat-manager configuration and systemd unit
  - name: flat-manager config
    template: src=./etc/releases-config.json.in dest=/etc/releases-config.json
    notify:
    - restart flat-manager

  - name: copy utilities
    copy: src=./{{item}} dest=/{{item}} mode=0755
    with_items:
      - usr/local/bin/prune-repo.sh

  - name: install systemd unit based on templates
    template: src=./{{item}}.in dest=/{{item}}
    with_items:
      - etc/systemd/system/flat-manager.service
      - etc/systemd/system/prune-repository.service
      - etc/systemd/system/prune-repository.timer

  - name: install systemd units
    copy: src=./{{item}} dest=/{{item}}
    with_items:
      - etc/systemd/system/dehydrated-renewel.service
      - etc/systemd/system/dehydrated-renewel.timer

  - name: enable systemd units
    systemd: name={{item}} enabled=yes daemon_reload=yes state=started
    with_items:
      - flat-manager.service
      - prune-repository.timer
      - dehydrated-renewel.timer
      - postgresql.service
      - nginx.service

  - name: create /etc/nginx/static directory
    file:
      path: /etc/nginx/static/
      state: directory

  - name: install static flatpakrepo file
    template: src=./etc/nginx/static/freedesktop-sdk.flatpakrepo.in dest=/etc/nginx/static/freedesktop-sdk.flatpakrepo
