# Freedesktop SDK release server

This repository caintains the ansible playbook for deploying our [ostree releases server](https://ostree.readthedocs.io/en/latest/manual/repository-management/).

## Ansible Host configuration

Firstly Ansible must know where the host machine exists and how to connect to it, this is done via the *hosts* file in this project.


## SSL certificates

To make our cache server easily accesible we configure a DNS that can be used by any projects to acess the cache.

Currently we use:

        releases.freedesktop-sdk.io

The certificates are automatically generated via the [Dehydrated client](https://github.com/dehydrated-io/dehydrated)/nginx configuration documented in
the Ansible scripts.

In order to obtain a VALID certificate you must ensure Dehydrated is configured with the official Lets Encrypt
URL, instead of staging(testing) URL, this can be changed via the config file, found under */etc/dehydrated/config.sh*

## Deployment
To deploy the Ansible you can run:

    ansible-playbook -i hosts ./release/playbook.yml -e 'ansible_python_interpreter=/usr/bin/python3' --vault-password-file 'foo-bar'

This will select the artifacts server from the *hosts* file and then run the steps documented inside *playbook.yml*

When deploying to production for freedesktop-sdk we need ensure that we can decrypt our public/private keys that artifact servers uses authenticate users, to do this you
must pass the ansible-vault password the the *--vault-password-file*, this file is currently controlled by adds68, jjardon, valentind.

## Customising this deployment 

  * TODO
